<?php
use \Mailjet\Resources;

include_once("include/required_plugins.php");
include_once("include/acf.php");
include_once("include/acf-block.php");
include_once("include/settings-gutenberg.php");
include_once("include/widget.php");
include_once("include/clean.php");
include_once("include/no-comment.php");
include_once("include/images.php");
include_once("include/custom-post-type.php");
include_once("include/breadcrumb.php");
include_once("include/menu.php");
include_once("include/enqueue_scripts.php");
include_once("include/form.php");
include_once("include/modale.php");
include_once("include/pagination.php");


// Adding excerpt for page
//add_post_type_support( 'page', 'excerpt' );

/**
 * Filter the excerpt length to 20 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
//add_filter( 'excerpt_length', function( $length ) { return 20; } );

function ihag_unregister_taxonomy(){
		register_taxonomy('post_tag', array());
		register_taxonomy('category', array());
	// add_post_type_support( 'page', 'excerpt' );
	// remove_post_type_support( 'page', 'thumbnail' );
}
add_action('init', 'ihag_unregister_taxonomy');

// Copier le contenu d'une page ou d'un post à la création d'une traduction
function cw2b_content_copy( $content ) {    
		if ( isset( $_GET['from_post'] ) ) {
				$my_post = get_post( $_GET['from_post'] );
				if ( $my_post )
						return $my_post->post_content;
		}
		return $content;
}
//add_filter( 'default_content', 'cw2b_content_copy' );

function my_pre_get_posts($query) {

	if ( ! is_admin() && $query->is_main_query() && $query->is_search() ) {
			$query->set('post_type', array("post", "rendez-vous", "solution"));
			$query->set('posts_per_page', -1);
	} 

}
//add_action( 'pre_get_posts', 'my_pre_get_posts' );

function nbTinyURL($url)  {
	$ch = curl_init();
	$timeout = 5;
	curl_setopt($ch,CURLOPT_URL,'http://tinyurl.com/api-create.php?url='.$url);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
}

// recupere le term(valeur) d'une taxonomy (ex: Brad Pitt est le term de la taxonomy actor)
function ihag_get_term($post, $taxonomy){
	
	if ( class_exists('WPSEO_Primary_Term') ):
	$wpseo_primary_term = new WPSEO_Primary_Term( $taxonomy, get_the_id( $post ) );
	$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
	$term = get_term( $wpseo_primary_term );
	if ( is_wp_error( $term ) ) {
		$term = get_the_terms($post, $taxonomy);
		return $term[0];
	}
	return $term;
 else:
	 $term = get_the_terms($post, $taxonomy);
	return $term[0];
 endif;
}


function target_main_category_query_with_conditional_tags( $query ) {
	if ( ! is_admin() && $query->is_main_query() ) {
			// Not a query for an admin page.
			// It's the main query for a front end page of your site.
			if ( is_tax('taxo_training') && isset($_GET['var_taxo_tag']) && !empty($_GET['var_taxo_tag']) ) {
					// It's the main query for a category archive.
					// Let's change the query for category archives.
				 $query->set( 'tax_query', array(
					array(
						'taxonomy' => 'taxo_tag',
						'field'    => 'term_id',
						'terms'    => $_GET['var_taxo_tag'],
					)));
				}
	}
}
add_action( 'pre_get_posts', 'target_main_category_query_with_conditional_tags' );


// fonction scroll
function ihag_scroll(WP_REST_Request $request){

	$offset = ( (int)sanitize_text_field( $_POST["page"] ) - 1) * get_option('posts_per_page' );
	$args = array(
		'posts_per_page' => get_option('posts_per_page' ),
		'post_type'   => $_POST['cpt'],
		'post_status' => 'publish',
		'offset'  => $offset,
	);


	if(isset($_POST["taxo"]) && !empty($_POST["taxo"]) && $_POST["taxo"] > 0 ){
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'taxo_'.$_POST['cpt'],
				'field'    => 'term_id',
				'terms'    => (int) sanitize_text_field($_POST["taxo"])
			),
		);
	}
	if(isset($_POST["taxo_tag_var"]) && !empty($_POST["taxo_tag_var"]) && $_POST["taxo_tag_var"] > 0 ){
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'taxo_tag',
				'field'    => 'term_id',
				'terms'    => (int) sanitize_text_field($_POST["taxo_tag_var"])
			),
		);
	}

	$custom_query = new WP_Query($args);
	if ( $custom_query->have_posts() ) : 
		while ( $custom_query->have_posts() ) : 
				$custom_query->the_post();
				get_template_part( 'template-parts/archive', get_post_type() );
		endwhile;
	endif;

	return new WP_REST_Response( NULL, 200 );
}


/*
* traitement du post du form de Contact
* enregistrement des values dans le custom post type
*/
add_action('rest_api_init', function() {
	register_rest_route( 'ihag', 'scroll',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'ihag_scroll',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
});

/*recupère l'url d'une video */
function retrieve_id_video($video){

	preg_match('/src="(.+?)"/', $video, $matches_url );
	$src = $matches_url[1];	

	preg_match('/embed(.*?)?feature/', $src, $matches_id );
	$id = $matches_id[1];
	$id = str_replace( str_split( '?/' ), '', $id );

	return $id;

}

function ihag_the_post_thumbnail( $size = 'medium', $attr = array() ){
	if ( has_post_thumbnail() ) :
		the_post_thumbnail($size, $attr);
	elseif (get_field('imageFallback', 'option')) :
		$image = get_field('imageFallback', 'option');
		echo wp_get_attachment_image( $image, $size );
	endif;
}

function ihag_get_attachment_image( $image, $size, $attr = '' ){
	if ( $image ) :
		echo wp_get_attachment_image( $image, $size, '', $attr );
	else :
		$image = get_field('imageFallback', 'option');
		echo wp_get_attachment_image( $image, $size, '', $attr );
	endif;
}

/**
 * Check if WooCommerce is activated
 */
if ( ! function_exists( 'is_woocommerce_activated' ) ) {
	function is_woocommerce_activated() {
		if ( class_exists( 'woocommerce' ) ) { return true; } else { return false; }
	}
}


add_action( 'woocommerce_after_checkout_form', 'bbloomer_disable_shipping_local_pickup' );
function bbloomer_disable_shipping_local_pickup( $available_gateways ) {
		
	 // Part 1: Hide shipping based on the static choice @ Cart
	 // Note: "#customer_details .col-2" strictly depends on your theme
 
	 $chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
	 $chosen_shipping = $chosen_methods[0];
	 if ( 0 === strpos( $chosen_shipping, 'local_pickup' ) ) {
	 ?>
			<script type="text/javascript">
				 jQuery('.woocommerce-shipping-fields').fadeOut();
			</script>
	 <?php  
	 } 
 
	 // Part 2: Hide shipping based on the dynamic choice @ Checkout
	 // Note: "#customer_details .col-2" strictly depends on your theme
 
	 ?>
			<script type="text/javascript">
				 jQuery('form.checkout').on('change','input[name^="shipping_method"]',function() {
						var val = jQuery( this ).val();
						if (val.match("^local_pickup")) {
								jQuery('.woocommerce-shipping-fields').fadeOut();
						} else {
								jQuery('.woocommerce-shipping-fields').fadeIn();
						}
				 });
			</script>
	 <?php
}

add_filter( 'woocommerce_shipping_fields', 'ihag_shipping_fields', 10, 1 );
function ihag_shipping_fields( $address_fields ) {

	$chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
	$chosen_shipping = $chosen_methods[0];
	if ( 0 === strpos( $chosen_shipping, 'local_pickup' ) ) {//Drive
		$address_fields['shipping_phone']['required'] = false;
		$address_fields['shipping_phone']['label'] = "Téléphone";
		$address_fields['shipping_first_name']['required'] = false;
		$address_fields['shipping_last_name']['required'] = false;
		$address_fields['shipping_address_1']['required'] = false;
		$address_fields['shipping_address_2']['required'] = false;
		$address_fields['shipping_state']['required'] = false;
		$address_fields['shipping_postcode']['required'] = false;
		$address_fields['shipping_city']['required'] = false;
		unset($address_fields['shipping_country']);
	}
	else{// livraison
		$address_fields['shipping_phone']['required'] = true;
		$address_fields['shipping_phone']['label'] = "Téléphone";
		//$address_fields['shipping_first_name']['required'] = true;
		//$address_fields['shipping_last_name']['required'] = true;
		//$address_fields['shipping_address_1']['required'] = true;
		//$address_fields['shipping_address_2']['required'] = false;
		//$address_fields['shipping_state']['required'] = true;
		//$address_fields['shipping_postcode']['required'] = true;
		//$address_fields['shipping_city']['required'] = true;
		//$address_fields['shipping_country']['required'] = true;

		//unset($address_fields['shipping_country']);
	}
	return $address_fields;
}



add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
function custom_override_checkout_fields( $fields ) {
		 $fields['order']['order_comments']['label'] = 'Consignes de livraison';
		 $fields['order']['order_comments']['placeholder'] = "particularité de la maison si lieu-dit, personne malentendante ou mettant du temps à se déplacer…";
		 
		 return $fields;
}


add_filter( 'woocommerce_billing_fields', 'ihag_billing_fields', 10, 1 );
function ihag_billing_fields( $address_fields ) {
	
	$address_fields['billing_address_1']['required'] = false;
	$address_fields['billing_address_2']['required'] = false;
	$address_fields['billing_state']['required'] = false;
	$address_fields['billing_postcode']['required'] = false;
	$address_fields['billing_city']['required'] = false;

	return $address_fields;
}


add_filter( 'woocommerce_shipping_calculator_enable_city', '__return_false' );
add_filter( 'woocommerce_shipping_calculator_enable_state', '__return_false' );

/**
 * Add the field to the checkout
 */

add_action( 'woocommerce_after_checkout_billing_form', 'my_custom_message_perso' );
function my_custom_message_perso( $checkout ) {
		echo '<div id="my_custom_message_perso"><h3>Message personnel</h3>';
		woocommerce_form_field( 'message_perso', array(
				'type'          => 'textarea',
				'required'		=> false,
				'class'         => array('form-row-wide'),
				'label'         => __('Message à joindre à la commande<br><small>Optionnel. La carte est gratuite avec chaque commande.</small>'),
				'placeholder'   => __('Ecrivez votre mot doux ici...'),
				), $checkout->get_value( 'message_perso' ));
		echo '</div>';
}

add_action( 'woocommerce_after_checkout_shipping_form', 'my_custom_message_ceremony' );
function my_custom_message_ceremony( $checkout ) {
	echo '<div id="my_custom_message_ceremony"><p><br><b><input type="checkbox" id="checkbox_message_ceremony_field" onclick="if(this.checked){document.getElementById(\'message_ceremony_field\').style.display=\'block\';}">
	<label for="checkbox_message_ceremony_field">Il s\'agit d\'une cérémonie</label></b></p>';
	woocommerce_form_field( 'message_ceremony', array(
			'type'          => 'textarea',
			'class'         => array('form-row-wide'),
			'label'         => __('Précisions de livraison'),
			'placeholder'   => __("S'il s'agit d'une livraison sur un lieu de cérémonie, merci d'indiquer l'heure précise de la cérémonie."),
			), $checkout->get_value( 'message_ceremony' ));
	echo '</div>';
}

add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );
function my_custom_checkout_field_update_order_meta( $order_id ) {
	if ( ! empty( $_POST['message_perso'] ) ) {
		update_post_meta( $order_id, 'message_perso', nl2br( $_POST['message_perso'] ) );
	}
	if ( ! empty( $_POST['message_ceremony'] ) ) {
		update_post_meta( $order_id, 'message_ceremony', nl2br( $_POST['message_ceremony'] ) );
	}
}

add_action('woocommerce_checkout_before_terms_and_conditions', 'checkout_additional_checkboxes');
function checkout_additional_checkboxes( ){
    $checkbox1_text = __( "Inscrivez-vous à la newsletter pour suivre l'actualité d'Anaya", "woocommerce" );
    ?>
    <p class="form-row custom-checkboxes">
        <label class="woocommerce-form__label checkbox custom-one" for="add_user_newsletter">
            <input type="checkbox" id="add_user_newsletter" value="register" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" name="add_user_newsletter" > <span><?php echo  $checkbox1_text; ?></span>
        </label>
    </p>
    <?php
}

add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');

function my_custom_checkout_field_process() {
    if ( ! empty( $_POST['add_user_newsletter'] ) ) {
		require dirname(__FILE__, 1).'/vendor/autoload.php';
		$mj = new \Mailjet\Client(get_field('id_mailjet', 'option'), get_field('mdp_mailjet', 'option'),true,['version' => 'v3']);
		$body = [
		'Action' => "addnoforce",
		'Contacts' => [
						[
						'Email' => $_POST['billing_email'],
						'IsExcludedFromCampaigns' => "false",
						'Name' => $_POST['billing_email'],
						'Properties' => "object"
						]
				]
		];
		$response = $mj->post(Resources::$ContactslistManagemanycontacts, ['id' => get_field('id_list', 'option'), 'body' => $body]);
		$response->success() && var_dump($response->getData());
	}
}

/*
add_action('add_meta_boxes','initialisation_metaboxes');
function initialisation_metaboxes(){
	add_meta_box('id_ma_meta', 'Renseigner Pré-état daté', 'SetPED', 'shop_order', 'side', 'high');
}
*/
/*
function SetPED($order){
	$order = new WC_Order($order->ID);
	if ( !empty($order) && !empty(get_post_meta( $order->ID, 'message_perso', true )) ) {
		$upload_dir = wp_upload_dir();
		echo '<p><b>Message</b> : '.get_post_meta( $order->ID, 'message_perso', true ).'<p>';
		echo '<p><a href="'.$upload_dir['baseurl'].'/message/message_'.$order->ID.'.pdf'.'" class="button" target="_BLANK">Imprimer le message</a></p>';    
	}
}*/
/*
add_action( 'save_post', 'so_payment_complete', 10,3 );
function so_payment_complete( $post_id, $post, $update){
	if ( !$update ){
			return;
	}
	if ( 'shop_order' === $post->post_type ) {
		require_once(__DIR__.'/vendor/fpdf/fpdf.php');
		$pdf = new FPDF();
		$pdf->AddPage();
		
	 // $pdf->AddFont('Anaya','',__DIR__.'/font/anaya.php');
		$pdf->SetFont('Arial','',16);
		$pdf->multicell(0,10, iconv('UTF-8', 'windows-1252',str_replace('<br />','',get_post_meta( $post_id, 'message_perso', true))), 1);
		//$pdf->Cell(40,20,"hello wordl");
		$upload_dir = wp_upload_dir();
		if(!is_dir($upload_dir['basedir'].'/message')){
			mkdir($upload_dir['basedir'].'/message');
		}
		$pdf->Output('F',$upload_dir['basedir'].'/message/message_'.$post_id.'.pdf');
		error_log($upload_dir['basedir'].'/message/message_'.$post_id.'.pdf');
	}
}*/

function remove_image_zoom_support() {
	remove_theme_support( 'wc-product-gallery-zoom' );
}
add_action( 'wp', 'remove_image_zoom_support', 100 );



add_filter( 'woocommerce_get_price_html', 'change_variable_products_price_display', 10, 2 );
function change_variable_products_price_display( $price, $product ) {

		// Only for variable products type
		if( ! $product->is_type('variable') ) return $price;

		$prices = $product->get_variation_prices( true );

		if ( empty( $prices['price'] ) )
				return apply_filters( 'woocommerce_variable_empty_price_html', '', $product );

		$min_price = current( $prices['price'] );
		$max_price = end( $prices['price'] );
		$prefix_html = '<span class="price-prefix">' . __('À partir de ') . '</span>';

		$prefix = $min_price !== $max_price ? $prefix_html : ''; // HERE the prefix

		return apply_filters( 'woocommerce_variable_price_html', $prefix . wc_price( $min_price ) . $product->get_price_suffix(), $product );
}

add_filter( 'woocommerce_loop_add_to_cart_link', 'ihag_remove_button_add_to_cart', 10 );
function ihag_remove_button_add_to_cart( ) {
		return;
}

remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
