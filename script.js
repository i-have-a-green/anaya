if (document.forms.namedItem("contactForm")) {
    document.forms.namedItem("contactForm").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("contactForm");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'contactForm', true);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessage').disabled = false;
                document.getElementById('sendMessage').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessage').classList.add("showResponseMessage");
            }
        };
        xhr.send(formData);
    });
}



// if (document.forms.namedItem("footer-newslettter-form")) {
//     document.forms.namedItem("footer-newslettter-form").addEventListener('submit', function(e) {
//         document.getElementById('sendMessageNewsletter').disabled = true; //limit le problème des doubles clic
//         e.preventDefault();

//         var form = document.forms.namedItem("footer-newslettter-form");
//         var formData = new FormData(form);
//         xhr = new XMLHttpRequest();
//         xhr.open('POST', resturl + 'formNewsletter', true);
//         xhr.onload = function() {
//             if (xhr.status === 200) {
//                 document.getElementById('sendMessageNewsletter').disabled = false;
//                 document.getElementById('sendMessageNewsletter').classList.add("hiddenSubmitButton");
//                 document.getElementById('ResponseMessageNewsletter').innerText = xhr.response.replace('"', '').replace('"', '');
//                 document.getElementById('ResponseMessageNewsletter').classList.add("showResponseMessage");
//             }
//         };
//         xhr.send(formData);
//     });
// }
document.addEventListener('DOMContentLoaded', function() {
    var js_upload_photos = document.getElementsByClassName('js_upload_photos');

    Array.prototype.forEach.call(js_upload_photos, function(bloc) {
        var btn = bloc.getElementsByClassName('more_photos');
        btn = btn[0];
        var new_photos = document.getElementById("img-listing_" + btn.id);
        console.log(btn.dataset.json);
        list_photos = JSON.parse(btn.dataset.json);
        new_photos.innerHTML = '';
        var increment = 0;
        if (list_photos.length > 0) {

            //à adapter
            var nbrPhoto = 3;
            if (window.matchMedia("(min-width: 600px)").matches) {
                /* La largeur minimum de l'affichage est 600 px inclus */
                nbrPhoto = 3;
            } else {
                /* L'affichage est inférieur à 600px de large */
                nbrPhoto = 2;
            }

            list_photos.forEach(function(photo) {

                if (increment < nbrPhoto) {
                    var image = document.createElement('img');
                    image.src = photo.src;
                    image.alt = photo.alt;
                    image.classList.add("img_photo_mariage");
                    image.dataset.src = photo.srclarge;
                    image.dataset.increment = increment;
                    image.dataset.jsonId = btn.id;
                    new_photos.appendChild(image);
                }
                increment++;
            });
        }
        img_photo_mariage();
    });


    Array.prototype.forEach.call(js_upload_photos, function(bloc) {
        var btn = bloc.getElementsByClassName('more_photos');
        btn = btn[0];
        btn.addEventListener('click', function(e) {
            btn.style.display = "none";
            btn.parentNode.classList.add("show-second-button");
            btn.parentNode.parentNode.classList.add("show-legend");
            list_photos = JSON.parse(btn.dataset.json);
            var new_photos = document.getElementById("img-listing_" + btn.id);
            new_photos.innerHTML = '';
            var increment = 0;
            if (list_photos.length > 0) {
                list_photos.forEach(function(photo) {
                    var image = document.createElement('img');
                    image.src = photo.src;
                    image.alt = photo.alt;
                    image.classList.add("img_photo_mariage");
                    image.dataset.src = photo.srclarge;
                    image.dataset.increment = increment;
                    image.dataset.jsonId = btn.id;
                    new_photos.appendChild(image);
                    increment++;
                });
            }
            img_photo_mariage();
        });

    });


});
/*

Animate header.php (see header.scss)

File structure :
------------------
1 - Open & Close menu
------------------
2 - Detect click on #overlay
------------------
3 - Scroll detection
------------------
// 4 - Modif topbar background on scroll

*/

// 1 - Open & Close menu (for mobile devices)
function toggleMenu() {
    document.getElementById("menu").classList.toggle("menu-open");
}

// 2 - Detect click on #overlay (for mobile menu)
// When we click outside of the mobile menu, the menu disappears
// We use #overlay to detect the click

/*document.addEventListener("DOMContentLoaded", function(event) {
    document.getElementById("overlay").addEventListener("click", function(e) {
        toggleMenu ();
    });
});*/

// 3 - Scroll detection
//topbar gets sticky on scroll
var scroll = document.getElementById("menu");
var menu = document.getElementById("menu");
var sticky = 0;
var isWhite = menu.dataset.white;

window.onscroll = function() {

    if (window.pageYOffset > sticky) {
        menu.classList.add("menu-small");
    } else {
        menu.classList.remove("menu-small");
    }
};


// 4 - Modif topbar background on scroll

/*window.onscroll = function() {

    var menu = document.getElementById("scroll-anchor");
    var header = document.getElementById("topbar-wrapper");
    var sticky = menu.offsetTop;

    if (window.pageYOffset > sticky) {
        header.classList.add("scroll");
        window.onscroll = function() { return; };
    } else {
        header.classList.remove("scroll");
    }
};*/

// var derniere_position_de_scroll_connue = 0;
// var ticking = false;
// var menu = document.getElementById("scroll-anchor");
// var header = document.getElementById("topbar-wrapper");
// var sticky = menu.offsetTop;

// window.addEventListener('scroll', function(e) {
//     if (!ticking) {
//         window.setTimeout(function() {
//             ticking = false;
//             if (window.scrollY > sticky) {
//                 header.classList.add("scroll");
//             } else {
//                 header.classList.remove("scroll");
//             }
//         }, 300); //fréquence du scroll
//     }
//     ticking = true;
// });
document.addEventListener('DOMContentLoaded', function() {
    img_photo_mariage();
});


function img_photo_mariage() {
    var increment = 0;
    var els = document.getElementsByClassName("img_photo_mariage");
    Array.prototype.forEach.call(els, function(el) {

        el.addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();

            close_modale();
            var modale = document.createElement("div");
            modale.classList.add("modale_photo");
            //modale.onclick = close_modale;

            var button = document.createElement("button");
            button.classList.add("closeModale");
            button.onclick = close_modale;
            button.appendChild(document.createTextNode("X"));
            modale.appendChild(button);

            var img = document.createElement("img");
            img.src = el.dataset.src;
            img.alt = el.alt;
            img.id = "photoModale";
            modale.appendChild(img);

            var prevButton = document.createElement("button");
            prevButton.id = "prevButton";
            prevButton.dataset.increment = el.dataset.increment;
            prevButton.dataset.jsonId = el.dataset.jsonId;
            prevButton.onclick = prevPhoto;
            prevButton.appendChild(document.createTextNode("<"));
            modale.appendChild(prevButton);

            var nextButton = document.createElement("button");
            nextButton.id = "nextButton";
            nextButton.dataset.increment = el.dataset.increment;
            nextButton.dataset.jsonId = el.dataset.jsonId;
            nextButton.onclick = nextPhoto;
            nextButton.appendChild(document.createTextNode(">"));
            modale.appendChild(nextButton);

            document.body.appendChild(modale);
        });
    });
}

function prevPhoto() {
    btn = document.getElementById(this.dataset.jsonId);
    increment = parseInt(this.dataset.increment) - 1;
    document.getElementById("nextButton").dataset.increment = increment;
    document.getElementById("prevButton").dataset.increment = increment;

    var list_photos = JSON.parse(btn.dataset.json);
    var photo = list_photos[increment];
    document.getElementById("photoModale").src = photo.srclarge;

    document.getElementById("nextButton").disabled = false;
    document.getElementById("prevButton").disabled = false;
    if (increment <= 0) {
        this.disabled = true;
    }
}

function nextPhoto() {
    btn = document.getElementById(this.dataset.jsonId);
    increment = parseInt(this.dataset.increment) + 1;
    document.getElementById("nextButton").dataset.increment = increment;
    document.getElementById("prevButton").dataset.increment = increment;

    var list_photos = JSON.parse(btn.dataset.json);
    var photo = list_photos[increment];
    document.getElementById("photoModale").src = photo.srclarge;

    document.getElementById("nextButton").disabled = false;
    document.getElementById("prevButton").disabled = false;
    if (increment >= (list_photos.length - 1)) {
        this.disabled = true;
    }
}

function close_modale() {
    if (document.getElementsByClassName("modale_photo").length > 0) {
        var modale_photo = document.getElementsByClassName("modale_photo");
        modale_photo[0].remove();
    }
}
document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("btn-modale");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();

            var modale = document.createElement("div");
            modale.classList.add("modale");
            modale.id = el.dataset.uniqId;

            var button = document.createElement("button");
            button.classList.add("closeModale");
            button.onclick = function() { document.getElementById(el.dataset.uniqId).remove(); };
            button.appendChild(document.createTextNode("X"));
            modale.appendChild(button);

            var modaleContent = document.createElement("div");
            modaleContent.id = 'modaleContent' + el.dataset.uniqId;
            modaleContent.classList.add("modaleContent");
            modale.appendChild(modaleContent);

            document.body.appendChild(modale);

            for (var key in iframe) {
                // check if the property/key is defined in the object itself, not in parent
                if (iframe.hasOwnProperty(key)) {
                    if (key == el.dataset.uniqId) {
                        document.getElementById('modaleContent' + el.dataset.uniqId).innerHTML = iframe[key];
                        modale.classList.add("active");
                    }
                }
            }
        });
    });
});
// organise - infinit scroll 
if (document.querySelector('#infinite-list')) {
    var base_url = window.location.href;
    var elm = document.querySelector('#infinite-list');
    // loader = document.querySelector('#loaderPost');
    var page = elm.dataset.page;
    var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    load = false;
    ticking = false;
    window.addEventListener('scroll', function(e) {
        if (!ticking) {
            window.setTimeout(function() {
                ticking = false;
                //Do something
                if (!load && (elm.offsetTop + elm.clientHeight) < (window.scrollY + height)) { //scroll > bas de infinite-list
                    //inserer les éléments suivants
                    if (page >= elm.dataset.nbPageMax) {
                        console.log(load);
                    } else {
                        load = true;
                        readMorePost();
                    }

                }
            }, 300); //fréquence du scroll
        }
        ticking = true;
    });
}



function readMorePost() {
    page++;
    // loader.classList.add("active");
    var formData = new FormData();
    formData.append("page", page);
    formData.append("cpt", elm.dataset.cpt);
    formData.append("taxo", elm.dataset.taxo);
    formData.append("taxo_tag_var", elm.dataset.taxo_tag);

    console.log("readMorePost");

    xhr = new XMLHttpRequest();
    xhr.open('POST', resturl + 'scroll', true);
    xhr.onload = function() {
        if (xhr.status === 200) {
            load = false;
            console.log(xhr.response);
            elm.insertAdjacentHTML('beforeend', xhr.response);
            // window.history.replaceState("", "", elm.dataset.url + "page/" + page + "/");
            // loader.classList.remove("active");
        }
    };
    xhr.send(formData);
}

if (document.getElementsByClassName("woocommerce-product-gallery__wrapper").length) {
    var els = document.getElementsByClassName("woocommerce-product-gallery__wrapper")[0].getElementsByTagName("a");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
        });
    });
}
document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("JSrslink");
    var heightScreen = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    var widthScreen = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.stopPropagation();
            e.preventDefault();
            var width = 320,
                height = 400,
                left = (widthScreen - width) / 2,
                top = (heightScreen - height) / 2,
                url = this.href,
                opts = 'status=1' +
                ',width=' + width +
                ',height=' + height +
                ',top=' + top +
                ',left=' + left;
            window.open(url, 'myWindow', opts);
            return false;
        });
    });

    copyLink = document.getElementsByClassName('copyLink');
    Array.prototype.forEach.call(copyLink, function(el) {
        el.addEventListener("click", function(e) {
            e.stopPropagation();
            copyLinkValue = el.getElementsByClassName('copyLinkValue');
            copyLinkValue[0].focus();
            copyLinkValue[0].select();
            document.execCommand("copy");
            el.classList.add('active');
        });

    });



});