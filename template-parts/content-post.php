<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */
?>

<?php // Post Title ?>
<header id="post<?php the_ID(); ?>" class="wrapper">
	<a class="link-discrete" href="<?php the_permalink();?>">
		<h2><?php the_title();?></h2>
	</a>

	<div class="entry-content">
		<?php
		$time_string = 'Le <time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		echo sprintf( $time_string,
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( DATE_W3C ) ),
			esc_html( get_the_modified_date() )
		);
		echo " par ".esc_html( get_the_author() );
		?>
	</div>

</header>

<?php 
// Post Content
if(is_single()): 
	echo '<main id="wp-content">';
	the_content();
	echo '</main>';
else:
	echo '<div class="wrapper">';
	the_excerpt();
	echo '</div>';
endif;
?>