<?php
/**
 * Block Name: Titre
 */
 ?>

<?php
$title = get_field('title');
$html = get_field('level_title');

if(get_field('is_black')) {
	$color = 'black';
} else {
	$color = 'green';
}


if ( empty($title) ):

	echo '<em>Renseigner le titre</em>';

else :

	echo "<". $html ." class='wp-block wrapper center wrapper-medium top-padding-regular ". $color ." '>". $title ."</". $html .">";
	echo '<img class="wp-block title-separator btm-padding-small" src="'.get_template_directory_uri().'/image/flower.png" alt="#" aria-hidden="true" width="45" height="16">';

endif; ?>


