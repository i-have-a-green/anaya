<?php
/**
 * Block Name: Membre de l'équipe
 */
 ?>

<?php 
// Preview in Gutenberg Admin
$is_preview = get_field('is_preview');
$is_preview_class = '';
if (!empty($is_preview)) {
	$is_preview_class = 'is_preview';
	$img_preview = get_field('imageFallback', 'option');
}
?>

<article class="wp-block blk-team h-padding-regular v-padding-small <?php echo $is_preview_class; ?> ">

<?php
$name = get_field('name');
$age = get_field('age');
$photo = get_field('photo');
$description = get_field('description');

if (empty($name)):
		echo '<em>Renseigner le titre du bloc</em>';
else :

	echo '<div class="team-card left">';


		// Photo
		if(!empty($photo)):

			echo '<div class="center has-gradient">';
				// Photo Caption (option)
				if (wp_get_attachment_caption($photo)) {
					echo '<p class="center body-like">'. wp_get_attachment_caption($photo) .'</p>';
				}
				// Photo
				echo wp_get_attachment_image($photo, 'team', '',  array( "class" => "center" ));
			echo '</div>';

		// PReview in Gutenberg Admin
		elseif (!empty($is_preview)) : 

			echo '<div class="center has-gradient">';
				echo wp_get_attachment_image($img_preview, 'team', '',  array( "class" => "center img-preview " ));
			echo '</div>';

		endif;

		// Title + Age
		if(!empty($age)):

			echo '<div class="has-subtitle">';
				echo '<h2 class="h1-like green no-margin left ">'. $name .'</h2>';
				echo '<p class="subtitle no-margin">'. $age .'</p>';
			echo'</div>';

		// Title Only
		else : 

		echo '<h2 class="h1-like green no-margin left ">'. $name .'</h2>';

		endif;

		// Content
		if(!empty($description)):
			echo '<div class="entry-content">'. $description .'</div>';
		endif;

	echo '</div>';

endif; 
?>

</article>
