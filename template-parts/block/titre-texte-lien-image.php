<?php
/**
 * Block Name: Titre-texte-lien-image
 */
 ?>




<?php 
// Variables
$setting_image = get_field('setting_image'); 
$setting_position = get_field('setting_content'); 
$mobile_image = get_field('mobile_image');
$standard_image = get_field('standard_image');

$id = 'section-' . $block['id'];

// Preview in Gutenberg Admin
$is_preview = get_field('is_preview');
$is_preview_class = '';
if ($is_preview) {
	$standard_image = get_field('imageFallback', 'option');
	$is_preview_class = 'is_preview';
}

	/* if need image decoration */
	if(get_field('need_decoration') == true) {
		$setting_decor = 'has-decor';
	}

	/* if need image gradient 
	if( $setting_image == 'medium_size' ) {
		$setting_gradient = 'has-gradient';
	}
	*/
?>

<?php

if ( empty($standard_image) ):
		echo '<p class="admin-label">Renseigner une image dans le bloc "Texte & image"</p>';
else :

	echo '<section id="'.esc_attr($id).'" class="wp-block blk-text-anaya center layout-'. $setting_image .' txt-'. $setting_position .' '. $is_preview_class .'">';

		// text container
		echo '<div class="txt-container">';
			
			$title = get_field('title');
			$level_title = get_field('level_title');
			$text = get_field('text');

			// title
			if ( $title && $level_title ):
				echo "<". $level_title ." class=' h1-like title'>". $title ."</". $level_title .">";
			endif; 

			// Decoration between title & text
			if ( $title && $level_title && $text ):
				echo '<img class="separator" src="'. get_template_directory_uri().'/image/flower.png" alt="#" aria-hidden="true" width="45" height="16">';
			endif; 

			// text
			if(!empty($text)):
				echo '<div class="entry-content no-useless-margin">'. $text .'</div>';
			endif; 

			// Link (option)
			$link = get_field('link');
			if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="button-brd" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
				<?php 
			endif; 

		// End of text container		
		echo '</div>';

		// Image container
		echo '<div class="image-container '.$setting_decor.'">';

			// Image decor
			if ($setting_decor) {
				if ($setting_position = 'right' ) {
					echo '<img class="decor" src="'. get_template_directory_uri().'/image/background-left.svg" alt="#" aria-hidden="true" width="260" height="390">';
				} else {
					echo '<img class="decor" src="'. get_template_directory_uri().'/image/background-right.svg" alt="#" aria-hidden="true" width="260" height="390">';
				}
			}

			/* if doesnt need mobile image */
			if( get_field('need_mobile_image') == false ){
				echo wp_get_attachment_image($standard_image, 'fullwidth', '', array( "class" => "visual" ));
			} else {
				/* if need mobile image */
				/* Show mobile image for mobile browser */
				if (wp_is_mobile()):
					echo wp_get_attachment_image($mobile_image, 'fullwidth-mobile', '', array( "class" => "visual" ));
				else:
				/* Show standard image for standard browser */
					echo wp_get_attachment_image($standard_image, 'fullwidth', '', array( "class" => "visual" ));
				endif;
			};

		echo '</div>';
		// End of image container	

	echo '</section>';

endif; 
?>


