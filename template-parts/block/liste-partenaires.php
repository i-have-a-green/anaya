<?php
/*
 * Block Name: Liste-partenaires
 */
 ?>

<?php 
// Preview in Gutenberg Admin
$is_preview = get_field('is_preview');
$is_preview_class = '';
if (!empty($is_preview)) {

    $img_preview = get_field('imageFallback', 'option');
    $name_preview = "Nom";
	$is_preview_class = 'is_preview';
}
?>

<section class="wp-block blk-partner wrapper v-padding-small center <?php echo $is_preview_class ?>">
    
    <?php 
    // Title
    $title = get_field('title');

    if($title):
        echo '<h2 class="h1-like wrapper-large green no-margin">'. $title. '</h2>';
        echo '<img class="title-separator" src="'.get_template_directory_uri().'/image/flower.png" alt="#" aria-hidden="true" width="45" height="16">';
    endif;

    // Logo list : check the rows
    if( have_rows('partners') ):

        echo '<ul class="reset wrapper-max grid-partner">';

        // Loop through rows.
        while( have_rows('partners') ) : the_row();

            // Load sub field value.
            $partner_logo = get_sub_field("partner_logo");
            $link = get_sub_field("partner_link"); 
            $name = get_sub_field("partner_name"); 

            if (!$partner_logo && !$name ):
                echo '<em>'; _e("Renseigner le nom ou l'image du partenaire", "anaya"); echo '</em>';
            else:

                echo '<li class="partner-single center">';

                    // Link (option)
                    if ($link) :
                        echo '<a class="link-discrete center" href="'.$link.'">';
                    else:
                        echo '<span class="link-discrete center">';
                    endif;

                        // Image
                        echo ihag_get_attachment_image( $partner_logo, 'partner' );

                        // Title (option)    
                        if ($name) :
                            echo '<img class="item-separator" src="'.get_template_directory_uri().'/image/flower.png" alt="#" aria-hidden="true" width="45" height="16">';
                            echo '<h3 class="body-like">'. $name .'</h3>';
                        endif;

                    // Close Link (option)
                    if ($link) :
                        echo '</a>';
                    else:
                        echo '</span>';
                    endif;

                echo '</li>';

            endif; 

        // End loop.
        endwhile;


    // Preview in Gutenberg Admin
    elseif ($is_preview):
        
        echo '<ul class="reset wrapper-max grid-partner grid-partner-preview">';

            echo '<li class="partner-single center">';
                echo wp_get_attachment_image(  $img_preview, 'partner' );                        
                echo '<img class="item-separator" src="'.get_template_directory_uri().'/image/flower.png" alt="#" aria-hidden="true" width="45" height="16">';
                echo '<h3 class="body-like">'. $name_preview .'</h3>';
            echo '</li>';

            echo '<li class="partner-single center">';
                echo wp_get_attachment_image(  $img_preview, 'partner' );                        
                echo '<img class="item-separator" src="'.get_template_directory_uri().'/image/flower.png" alt="#" aria-hidden="true" width="45" height="16">';
                echo '<h3 class="body-like">'. $name_preview .'</h3>';
            echo '</li>';

            echo '<li class="partner-single center">';
                echo wp_get_attachment_image(  $img_preview, 'partner' );                        
                echo '<img class="item-separator" src="'.get_template_directory_uri().'/image/flower.png" alt="#" aria-hidden="true" width="45" height="16">';
                echo '<h3 class="body-like">'. $name_preview .'</h3>';
            echo '</li>';

            echo '<li class="partner-single center">';
                echo wp_get_attachment_image(  $img_preview, 'partner' );                        
                echo '<img class="item-separator" src="'.get_template_directory_uri().'/image/flower.png" alt="#" aria-hidden="true" width="45" height="16">';
                echo '<h3 class="body-like">'. $name_preview .'</h3>';
            echo '</li>';       

    else : 

        echo '<em>'; _e("Renseigner un partenaire", "anaya"); echo '</em>';

    endif; ?>

</section>