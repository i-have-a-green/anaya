<?php
/**
 * Block Name: Mariage
 */
?>
<?php //$block_uniq_id = "id_".uniqid(); ?>

<?php 
// Preview in Gutenberg Admin
$is_preview_class = '';
$is_preview = get_field('is_preview');
if (!empty($is_preview)) {

	$img_preview = get_field('imageFallback', 'option');
	$is_preview_class = 'is_preview';
}
?>

<section class="wp-block blk-mariage js_upload_photos v-padding-regular <?php echo $is_preview_class ?> ">

<?php
$names = get_field('names');
$title = get_field('text');

if ( empty($names) ):

	echo '<em>Renseigner le titre du bloc mariage</em>';

else :
	
	echo '<div class="text-container wrapper wrapper-max">';
    
        // Decorative Title
        echo '<p>';
            echo '<span class="body-like">'; _e('Le mariage de', 'anaya'); echo '</span>';
            echo '<span class="h1-like green">'. $names .'</span>';
        echo '</p>';

        // Real Title (looks like a paragraph)
        if($title):
            echo '<h2 class="quote-like body-like entry-content">'. $title .'</h2>';
        endif;

    echo '</div>';

	// Images - Check rows exists :
	if( have_rows('photos') ):

		$uniq_id = uniqid();
		echo '<div class="wrapper first_photos img-listing wrapper-max" id="img-listing_'.$uniq_id.'">';
                
            $tab_photos = array();
            $i = 1;
            $size = 'mariage';

			// Loop through rows.
			while( have_rows('photos') ) : the_row();

				// Load sub field value.
				$photo = get_sub_field("photo");

				// Display first images
				/*if($i <= $visible_img ): ?>
					<img class="img_photo_mariage" src="<?php echo $photo['sizes'][$size]; ?>" data-increment="<?php echo $i -1;?>" data-src="<?php echo $photo['sizes']["large"]; ?>" alt="<?php echo $photo['alt'];?>">
				<?php	
				endif;*/

				$tab_photos[] = array(
					"src" 		=> $photo['sizes'][$size],
					"srclarge" 	=>$photo['sizes']["large"],
					//"alt" 		=> $photo['alt'],
				);
				
				
			$i++;
			 
			// End loop.
			endwhile;

		echo '</div>';
		?>

		<?php // New photos are created here (when click on button "more_photos") ?>
		<!--<div class="wrapper img-listing wrapper-max new_photos items-<?php // echo $visible_img;?> "></div>-->

		<?php 
		// Legend
		if (get_field('legend')) :
			echo '<div class="mariage-legend wrapper entry-content wrapper-max">'. get_field('legend') .'</div>';
		endif;
		?>
				
		<div class="button-container wrapper">

			<?php // Button -> Load more photos ?>
			<button class='reset-style more_photos' id="<?php echo $uniq_id;?>" data-json='<?php echo json_encode($tab_photos);?>' >
				<span><?php _e('En voir plus', 'anaya') ?></span>
				<img src="<?php echo get_template_directory_uri();?>/image/arrow-down.svg" alt="Charger plus de photos" width="45" height="45">
			</button>
		
			<?php // Button -> Show / hide loaded photos ?>
			<button class="reset-style toggle-img" onclick="this.classList.toggle('hide-img'); this.parentNode.classList.toggle('overlay-effect'); this.parentNode.parentNode.classList.toggle('hide-new-photo');this.parentNode.parentNode.classList.toggle('show-legend');">
				<span class="hide-label"><?php _e('En voir moins', 'anaya') ?></span>
				<span class="show-label" ><?php _e('En voir plus', 'anaya') ?></span>
				<img src="<?php echo get_template_directory_uri();?>/image/arrow-down.svg" alt="Afficher / Cacher les images" width="45" height="45">
			</button>

		</div>

	<?php 
	
	else :

		if($img_preview) :

			echo '<div class="wrapper first_photos img-listing items-3 wrapper-max">';
			
				$size = 'mariage';
				echo wp_get_attachment_image($img_preview, $size);
				echo wp_get_attachment_image($img_preview, $size);
				echo wp_get_attachment_image($img_preview, $size);

			echo '</div>';

			echo '<div class="button-container-preview">';
				echo '<img src="'. get_template_directory_uri() .'/image/preview_blk_mariage.png" alt="#" width="300" height="53">';
			echo '</div>';

		endif;
	
	endif;
	
endif; 
?>

</section>