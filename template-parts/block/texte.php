<?php
/**
 * Block Name: Texte
 */
 ?>

<section class="blk-text">

<?php
$texte = get_field('text');

if ( empty($texte) ):?>
		<em>Renseigner le bloc</em>
<?php else :?>

	<div class="narrow-wrapper v-padding-regular">

		<?php if(!empty(get_field('title'))):?>
			<h2><?php the_field('title');?></h2>
		<?php endif; ?>

		<div class="entry-content"><?php the_field('text');?></div>
		
		<?php 
		$link = get_field('link');
		if( $link ): 
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';
			?>
			<a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
		<?php endif; ?>


	</div><!-- /wrapper -->

<?php endif; ?>

</section>
