<?php
/**
 * Block Name: Bloc FAQ
 */
 ?>

<section class="wp-block layout-medium_size">

<?php
$faq = get_field('faq');

if ( empty($faq) ):

	echo '<em>Renseigner le bloc</em>';

else :

	// Title
	echo '<h2 class="h3-like wrapper-medium is-centered"></h2>';

	if( have_rows('faq') ):

		// Loop through rows.
		while( have_rows('faq') ) : the_row();
		
			$question = get_sub_field('question');
			$answer = get_sub_field('answer');
			if(!empty ($question && $answer) ):
			
				echo '<details class="wrapper-medium is-centered ">';
					echo '<summary class="green" >'. $question .'</summary>';
					echo '<div class="entry-content">'. $answer.'</div>';
				echo '</details>';

			endif;

		endwhile;

	endif;

endif; 
?>

</section>
