<?php
/**
 * Block Name: Bloc Image
 */
 ?>

<div class="wp-block wrapper center v-padding-small">

<?php
$image = get_field('image');
$size = 'fullwidth-mobile';
$is_preview = get_field('is_preview');

if ($is_preview) {
	$image = $img_preview = get_field('imageFallback', 'option');
}

if ( !$image ):

	echo '<em>Renseigner l\'image</em>';

// Preview in Gutenberg Admin
elseif ( $is_preview ) :

	echo '<div class="wrapper-large">';
		echo wp_get_attachment_image($image, $size);
		echo '<p class="center center body-like no-margin">'. get_field('text') .'</p>';
	echo '</div>';

else :

	echo '<div class="wrapper-large">';

		echo wp_get_attachment_image($image, $size);

		// Photo Caption (option)
		if (wp_get_attachment_caption($image)) {
			echo '<p class="center center body-like no-margin">'. wp_get_attachment_caption($image) .'</p>';
		}
	
	echo '</div>';


endif; 
?>
</div>
