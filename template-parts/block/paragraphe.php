<?php
/**
 * Block Name: Paragraphe
 */
 ?>

<?php
$texte = get_field('text');

if ( empty($texte) ):

	echo '<em class="center">Renseigner le paragraphe</em>';

else :

	echo '<div class="entry-content">'.$texte.'</div>';
endif; 
?>
