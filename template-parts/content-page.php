<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */
?>

<?php
if (get_field('bg_position')) {
	$bg_position = "bg-right";
} else {
	$bg_position = "bg-left";
}
?>

<main id="wp-content" class="bg-pattern <?php echo $bg_position ?>" ><!-- Page-ID: <?php the_ID(); ?> -->

	<?php the_content(); ?>

</main>
