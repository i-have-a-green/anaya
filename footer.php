	</div><!-- div #content -->
	
	<!-- Icones - garanties -->
		<?php 
		// Preview in Gutenberg Admin
		$is_preview = get_field('is_preview');
		if (!empty($is_preview)) {

			$img_preview = get_field('imageFallback', 'option');
			$title_preview = "Nom";
			$is_preview_class = 'is_preview';
		}
		?>


		<div class="wp-block blk-icones wrapper v-padding-small center <?php echo $is_preview_class ?>">
			
		<?php 
		// Check rows exists.
		if( have_rows('icons', 'option') ):

			echo '<ul class="reset">';

				// Loop through rows.
				while( have_rows('icons', 'option') ) : the_row();

					// Load sub field value.
					$text = get_sub_field('text', 'option');
					$icon = get_sub_field("icon", 'option');
					$size= 'icon';

					if( $icon || $text ) :

						echo '<li>';

							// 1 - Image
							echo ihag_get_attachment_image($icon, $size);

							// 2 - Text
							if( !empty($text) ):
								echo '<h2 class="no-margin body-like green">'. $text .'</h2>';
							endif;  

						echo '</li>';

					else :

						echo '<em>'; _e("Renseigner un texte ou une image", "anaya"); echo '</em>';

					endif;  

				// End loop.
				endwhile;

			echo '</ul>';

		// Preview in Gutenberg Admin
		elseif ($is_preview) : 

			echo '<ul class="reset">';

				echo '<li>';
					echo ihag_get_attachment_image($img_preview, $size);
					echo '<h2 class="no-margin body-like green">'. $text_preview .'</h2>'; 
				echo '</li>';
				
				echo '<li>';
					echo ihag_get_attachment_image($img_preview, $size);
					echo '<h2 class="no-margin body-like green">'. $text_preview .'</h2>'; 
				echo '</li>';

				echo '<li>';
					echo ihag_get_attachment_image($img_preview, $size);
					echo '<h2 class="no-margin body-like green">'. $text_preview .'</h2>'; 
				echo '</li>';

				echo '<li>';
					echo ihag_get_attachment_image($img_preview, $size);
					echo '<h2 class="no-margin body-like green">'. $text_preview .'</h2>'; 
				echo '</li>';

			echo '</ul>';    

		else : 

			echo '<em>'; _e("Renseigner un item", "anaya"); echo '</em>';

		endif;
		?>
		
		</div>
		<!-- End - Icones - Garanties -->


	<footer id="footer" class="wrapper">

		<div id="footer-layout" class="wrapper-large v-padding-small">

			<!-- Footer Part 1 : Logo + RS -->
			<a id="footer-logo" class="link-discrete black center" href="<?php echo home_url() ?>">
				<span class="sr-only"><?php esc_html_e('Lien vers la page d\'accueil', 'ihag')?></span>
				<?php 
				$logo = get_field('logo_footer', 'options');
				if ($logo) {
					$size = 'logo';
					echo wp_get_attachment_image($logo, $size);
				} else {
					echo '<span class="h3-like black font-title"><'. get_bloginfo ( 'name' ) .'</span>';
				}
				?>
			</a>

			<!-- Footer Part 2 : Text -->
			<?php 
			$text_footer = get_field('content', 'option');
			if ($text_footer) { 
				echo '<div id="footer-content" class="entry-content">'. $text_footer .'</div>';	
			}
			?>
			<!-- Footer Part 3 : Social Media -->
			<nav id="footer-social" class="center">
				<ul>
					<?php 
						$facebook = get_field('facebook', 'option');
						$pinterest = get_field('pinterest', 'option');
						$instagram = get_field('instagram', 'option');
						$twitter = get_field('twitter', 'option');
						$linkedin = get_field('linkedin', 'option');
						$youtube = get_field('youtube', 'option');

						if( $facebook ):
							$link = $facebook;
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<li>
								<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
									<span class="sr-only"><?php echo esc_html( $link_title ); ?></span>
									<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/facebook.svg" height="34" width="34">
								</a>
							</li>
						<?php 
						endif; 

						if( $pinterest ):
							$link = $pinterest;
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<li>
								<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
									<span class="sr-only"><?php echo esc_html( $link_title ); ?></span>
									<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/pinterest.svg" height="34" width="34">
								</a>
							</li>
						<?php 
						endif; 

						if( $instagram ):
							$link = $instagram;
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<li>
								<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
									<span class="sr-only"><?php echo esc_html( $link_title ); ?></span>
									<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/instagram.svg" height="34" width="34">
								</a>
							</li>
						<?php 
						endif; 

						if( $twitter ):
							$link = $twitter;
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<li>
								<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
									<span class="sr-only"><?php echo esc_html( $link_title ); ?></span>
									<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/twitter.svg" height="34" width="34">
								</a>
							</li>
						<?php 
						endif;
						
						if( $linkedin ):
							$link = $linkedin;
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<li>
								<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
									<span class="sr-only"><?php echo esc_html( $link_title ); ?></span>
									<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/linkedin.svg" height="34" width="34">
								</a>
							</li>
						<?php 
						endif;

						if( $youtube ):
							$link = $youtube;
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<li>
								<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
									<span class="sr-only"><?php echo esc_html( $link_title ); ?></span>
									<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/youtube.svg" height="34" width="34">
								</a>
							</li>
						<?php 
					endif;
					?>

				</ul>
			</nav>

			<!-- Part 4 Menu -->
			<div id="footer-menu">
				<p class="uppercase bold"><?php esc_html_e( 'Informations pratiques', 'ihag' ); ?></p>
				<?php echo ihag_menu('secondary'); ?>
			</div>

		</div>

		<?php
		$signature = get_field('signature', 'options');
		if($signature) {
			echo '<aside class="wrapper-large btm-padding-small center">';
				echo'<div class="black entry-content center">'.$signature.'</div>';
			echo '</aside>';
		}
		?>
			
	</footer><!-- End of #footer -->

<?php wp_footer(); ?>

</body>
</html>
