<?php get_header(); ?>


<?php 
// Begining of the loop 
if (have_posts()) : 

	while ( have_posts() ) :
		the_post();
		get_template_part( 'template-parts/content', get_post_type() );
	endwhile;
	
endif;
//End of the loop
?>

<?php
get_footer();
