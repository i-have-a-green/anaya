<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php do_action( 'wpo_wcpdf_before_document', $this->type, $this->order ); ?>

<!-- $id_commande -->
<?php 
$id_commande = $order->get_id();

// delivery infos 
//(strftime('%d %B %Y', strtotime($delivery_date))); 
$delivery_date =  date_i18n("l j F Y", strtotime(get_post_meta($id_commande, '_delivery_date', true)));
$delivery_hours = get_post_meta( $id_commande, '_delivery_time_frame', true );
?>

<table class="head container">
	<tr>
		<!-- Logo -->
		<td class="header" width="33%">
		<?php
		if( $this->has_header_logo() ) {
			$this->header_logo();
		} else {
			echo $this->get_title();
		}
		?>
		</td>

		<!-- Infos expéditeur -->
		<td class="shop-info">
			<h2 class="order-number"><span>N° de commande : <?php $this->order_number(); ?></span></h2>
			<?php do_action( 'wpo_wcpdf_before_shop_name', $this->type, $this->order ); ?>
			<div class="shop-name"><h3><?php $this->shop_name(); ?></h3></div>
			<?php do_action( 'wpo_wcpdf_after_shop_name', $this->type, $this->order ); ?>
			<?php do_action( 'wpo_wcpdf_before_shop_address', $this->type, $this->order ); ?>
			<div class="shop-address"><?php $this->shop_address(); ?></div>
			<?php do_action( 'wpo_wcpdf_after_shop_address', $this->type, $this->order ); ?>
		</td>
		
		<!-- Date et horaire de livraison -->
		<td class="shipping-datetime">
			<h3 style="text-decoration: underline;"><span>Date d'expedition </span></h3>
			<div> - <?php echo $delivery_date ;?></div>
			<div> - Entre <b><?php echo $delivery_hours['time_from']; ?></b> et <b><?php echo $delivery_hours['time_to']; ?></b></div>
			<?php
			add_action( 'wpo_wcpdf_after_order_data', 'wpo_wcpdf_delivery_date', 10, 2 );
			function wpo_wcpdf_delivery_date ($template_type, $order) {
				if ($template_type == 'packing-slip') {
					// get the delivery date from the order
					$delivery_date = $order->get_meta('delivery_date');
					// convert the delivery date to a human readable format (using the WooCommerce/WordPress date format settings)
					$formatted_delivery_date = date_i18n( wc_date_format(), $delivery_date );
					?>
					<tr class="delivery-date">
						<th>Date d'expédition :</th>
						<td><?php echo $delivery_date; ?></td>
					</tr>
					<?php
				}
			}
			?>
			<?php if(!empty(get_post_meta( $id_commande, 'message_ceremony', true ))): ?>
				<br>
				<h3><span> Cérémonie : </span></h3>
				<div><?php echo(get_post_meta( $id_commande, 'message_ceremony', true )); ?></div>
			<?php endif; ?>
		</td>
	</tr>
</table>

<!-- Titre du document -->
<h1 class="document-type-label">
<?php if( $this->has_header_logo() ) echo $this->get_title(); ?>
</h1>

<?php do_action( 'wpo_wcpdf_after_document_label', $this->type, $this->order ); ?>

<table class="order-data-addresses">
	<tr>
		<!-- Coordonnées client / livraison -->
		<td class="address shipping-address">
			<h3>Destinataire :</h3>
			<?php do_action( 'wpo_wcpdf_before_shipping_address', $this->type, $this->order ); ?>
			<?php $this->shipping_address(); ?>
			<?php do_action( 'wpo_wcpdf_after_shipping_address', $this->type, $this->order ); ?><br>
			<!-- <?php //if ( isset($this->settings['display_email']) ) { ?>
			<div class="billing-email"><?php //$this->billing_email(); ?></div>
			<?php //} ?> -->
			<?php //var_dump($this->settings[0]); ?>
			<?php //if ( isset($this->settings['display_phone']) ) { ?>
			Tel: <?php echo (get_post_meta($id_commande, '_shipping_phone', true )); ?><br>

			<!-- <div class="shipping-phone"><?php //$this->shipping_phone(); ?></div> -->
			<?php //} ?>
		</td>
		<!-- Coordonnées facturation -->
		<td class="address billing-address">
			<?php if ( !empty($this->settings['display_billing_address']) && ( $this->ships_to_different_address() || $this->settings['display_billing_address'] == 'always' ) ) { ?>
				<h3>Coordonnées de facturation :</h3>
				<?php do_action( 'wpo_wcpdf_before_billing_address', $this->type, $this->order ); ?>
				<?php $this->billing_address(); ?>
				<?php do_action( 'wpo_wcpdf_after_billing_address', $this->type, $this->order ); ?>
			<?php } else { ?>
				<h3>Client :</h3>
				<?php echo (get_post_meta($id_commande, '_billing_first_name', true )); ?>
				<?php echo (get_post_meta($id_commande, '_billing_last_name', true )); ?><br>
				Tel: <?php echo (get_post_meta($id_commande, '_billing_phone', true )); ?><br>

				<?php //do_action( 'wpo_wcpdf_before_billing_address', $this->type, $this->order ); ?>
				<?php //var_dump($this); ?>
			<?php } ?>
		</td>
		<!-- Ref pour la commande -->
		<td class="order-data">
			<table>
				<?php do_action( 'wpo_wcpdf_before_order_data', $this->type, $this->order ); ?>
				<tr class="order-number">
					<th>N° de commande:</th>
					<td><?php $this->order_number(); ?></td>
				</tr>
				<tr class="order-date">
					<th><?php _e( 'Order Date:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<td><?php $this->order_date(); ?></td>
				</tr>
				<tr class="shipping-method">
					<!-- <th><?php// _e( 'Shipping Method:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th> -->
					<th>Mode d'expédition:</th>
					<td><?php $this->shipping_method(); ?></td>
				</tr>
				<?php //do_action( 'wpo_wcpdf_after_order_data', $this->type, $this->order ); ?>
			</table>			
		</td>
	</tr>
</table>

<?php do_action( 'wpo_wcpdf_before_order_details', $this->type, $this->order ); ?>

<!-- Tableau de la commande -->
<table class="" width="100%">
	
		<!-- Informations -->
		<tr style="background-color:#eee">
			<th class="product"><?php _e('Product', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="quantity"><?php _e('Quantity', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="prix"><?php _e('Prix', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
		</tr>
	
	<tbody>
		<?php $items = $this->get_order_items(); if( sizeof( $items ) > 0 ) : foreach( $items as $item_id => $item ) : ?>

		<tr class="<?php echo apply_filters( 'wpo_wcpdf_item_row_class', $item_id, $this->type, $this->order, $item_id ); ?>" >
			<!-- Produits -->
			<td class="product" style="border-bottom:solid 1px #ccc" width="35%">
				<?php //$description_label = __( 'Description', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
				<span class="item-name"><?php echo $item['name']; ?></span>
				<?php do_action( 'wpo_wcpdf_before_item_meta', $this->type, $item, $this->order  ); ?>
				<span class="item-meta"><?php echo $item['meta']; ?></span>
				<?php do_action( 'wpo_wcpdf_after_item_meta', $this->type, $item, $this->order  ); ?>
			</td>
			<!-- Quantité -->
			<td class="quantity" style="border-bottom:solid 1px #ccc" width="15%"><?php echo $item['quantity']; ?></td>
			<!-- Prix -->
			<td class="prix" style="border-bottom:solid 1px #ccc" width="50%">
				<?php echo ($item['price']);?>
			</td>

		</tr>
		<?php endforeach; endif; ?>

		<tr>
			<td colspan="2"></td>
			<td>
				<table>
						<?php foreach( $this->get_woocommerce_totals() as $key => $total ) : 
							if($key != 'payment_method'):	
						?>
								<tr class="<?php echo $key; ?>">
									<th class="description"  width="50%"><?php echo $total['label']; ?></th>
									<td class="price"  width="50%"><span class=“totals-price”><?php echo $total['value']; ?></span></td>
								</tr>

							<?php endif; 
						endforeach; ?>
				</table>
			</td>
		</tr>
	
	</tbody>
</table>

<div class="bottom-spacer"></div>

<?php do_action( 'wpo_wcpdf_after_order_details', $this->type, $this->order ); ?>


<?php do_action( 'wpo_wcpdf_after_customer_notes', $this->type, $this->order ); ?>
	
	<!-- Elements détachables  -->
	<table width="100%" style="border-top: black 1px dotted; ">
		<tr>
			<td>
				<b>Informations d'expédition</b><br>
				N° de commande : <?php $this->order_number(); ?><br>
				<!-- Date de la commande : <?php //$this->order_date(); ?><br> -->
				<b>Date d'expédition : <?php echo $delivery_date ;?></b><br>
				Entre <?php echo $delivery_hours['time_from']; ?> et <?php echo $delivery_hours['time_to']; ?><br>
						
			</td>
			<td>
				<b>Liste produits : </b><br>
				<ul class="">
				<?php $items = $this->get_order_items(); if( sizeof( $items ) > 0 ) : foreach( $items as $item_id => $item ) : ?>
					<li class="product">
						<span class="item-name"><?php echo $item['quantity']; ?> - <?php echo $item['name']; ?> </span>
						<span class="item-meta"><?php echo $item['meta']; ?></span>
						
					</li>
				<?php endforeach; endif; ?>
				</ul>
			</td>	
						
		</tr>
		<tr>
			<td>
				<?php do_action( 'wpo_wcpdf_before_customer_notes', $this->type, $this->order ); ?>
				<?php if ( $this->get_shipping_notes() ) : ?>
					<b><?php _e( 'Customer Notes', 'woocommerce-pdf-invoices-packing-slips' ); ?> : </b>
					<?php $this->shipping_notes(); ?><br></br>
				<?php endif; ?>

				<?php if(!empty(get_post_meta($id_commande, 'message_ceremony', true ))): ?>
					<b>Cérémonie :</b><br>
					<?php echo(get_post_meta($id_commande, 'message_ceremony', true )); ?>
				<?php endif; ?>
			</td>
			<td>
				<b>Destinataire :</b><br>
					<?php do_action( 'wpo_wcpdf_before_shipping_address', $this->type, $this->order ); ?>
					<?php $this->shipping_address(); ?><br>
					<b>TEL : <?php echo get_post_meta($id_commande, '_shipping_phone', true) ; ?></b>
			</td>
		</tr>
		<tr>
			<td style="border-top: black 1px dotted; padding:16px; width:50%" >
				<b>Signature : </b>
				<div style="width:290px;height:100px;border:solid 1px #999;"></div>
			</td>
			<td style="border-top: black 1px dotted;padding:16px;border-left: black 1px dotted; font-size:14px;">
				<b>Message : </b><br>
				<?php echo (get_post_meta($id_commande, 'message_perso', true )); ?>
			</td>
		</tr>
	</table>


<?php//var_dump((int)$this->order_number());?>
<?php //var_dump(get_post_meta( (int)$this->order_number(), 'message_perso', true )); ?>

<?php if ( $this->get_footer() ): ?>
<div id="footer">
	
	<?php $this->footer(); ?>
</div><!-- #letter-footer -->
<?php endif; ?>

<?php do_action( 'wpo_wcpdf_after_document', $this->type, $this->order ); ?>