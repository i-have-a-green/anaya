<?php
function my_plugin_block_categories( $categories, $post ) {
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'anaya',
                'title' => __( 'Anaya', 'anaya' ),
                'icon'  => 'star-empty'
            ),
        )
    );
}
add_filter( 'block_categories', 'my_plugin_block_categories', 10, 2 );


add_action('acf/init', 'acf_init_blocs');
function acf_init_blocs() {
    if( function_exists('acf_register_block_type') ) {

        // Basic Blocks

        acf_register_block_type(
            array(
                'name'				    => 'titre',
                'title'				    => __('Titre'),
                'description'		    => __('Titre de niveau h1, h2, h3 ou h4'),
                'placeholder'		    => __('Titre'),
                'render_template'	    => 'template-parts/block/titre.php',
                'category'			    => 'anaya',
                'mode'                  => 'edit',
                'icon'				    => 'heading',
                'keywords'			    => array('titre', 'text'),
                'supports'	            => array(
                                            'align'	=> false,
                                            'mode'  => true,
                                        ),
                'example'               => array(
                                            'attributes' => array(
                                                'mode' => 'preview',
                                                'data' => array(
                                                'title'            => "Titre personnalié",
                                                'level_title'      => "h2",
                                                )
                                            )
                                        )
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'paragraphe',
                'title'				    => __('Paragraphe'),
                'description'		    => __('Bloc de texte avec un éditeur de contenus'),
                'placeholder'		    => __('Paragraphe'),
                'render_template'	    => 'template-parts/block/paragraphe.php',
                'category'			    => 'anaya',
                'mode'                  => 'edit',
                'icon'				    => 'editor-paragraph',
                'keywords'			    => array('paragraphe', 'text'),
                'supports'	            => array(
                                            'align'	=> false,
                                            'mode'  => true,
                                        ),
                'example'               => array(
                                            'attributes' => array(
                                                'mode' => 'preview',
                                                'data' => array(
                                                'text'            => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                                                )
                                            )
                                        )
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'image',
                'title'				    => __('Image'),
                'description'		    => __('Affiche une image si elle a été renseignée'),
                'placeholder'		    => __('image'),
                'render_template'	    => 'template-parts/block/image.php',
                'category'			    => 'anaya',
                'mode'                  => 'edit',
                'icon'				    => 'format-image',
                'keywords'			    => array('agence', 'bloc', 'image'),
                'supports'	            => array(
                                            'align'	=> false,
                                            'mode'  => true,
                                        ),
                'example'               => array(
                                            'attributes' => array(
                                                'mode' => 'preview',
                                                'data' => array(
                                                'is_preview'      => true,
                                                'text'            => "Légende",
                                                )
                                            )
                                        )
            )
        );

        // Anaya Blocks

        acf_register_block_type(
            array(
                'name'				    => 'titre-texte-lien-image',
                'title'				    => __('Texte & Image'),
                'description'		    => __('Bloc multifonction qui affiche du texte et une image'),
                'placeholder'		    => __('Texte & Image'),
                'render_template'	    => 'template-parts/block/titre-texte-lien-image.php',
                'category'			    => 'anaya',
                'mode'                  => 'edit',
                'icon'				    => 'cover-image',
                'keywords'			    => array('titre', 'text', 'lien', 'image', 'mise en page'),
                'supports'	            => array(
                                            'align'	=> false,
                                            'mode'  => true,
                                        ),
                'example'               => array(
                                            'attributes' => array(
                                                'mode' => 'preview',
                                                'data' => array(
                                                  'is_preview'      => true,
                                                  'title'           => "Titre",
                                                  'level_title'     => "h2",
                                                  'text'            => "Lorem ispum dolores sid amet cumque.",
                                                  'setting_content' => "right",
                                                  'setting_image'   => "medium_size",
                                                )
                                            )
                                        )                     
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'mariage',
                'title'				    => __('Mariage'),
                'description'		    => __('Bloc qui affiche les noms des mariés et les photos du mariage.'),
                'placeholder'		    => __('Mariage'),
                'render_template'	    => 'template-parts/block/mariage.php',
                'category'			    => 'anaya',
                'mode'                  => 'edit',
                'icon'				    => 'heart',
                'keywords'			    => array('mariage', 'mariés', 'photo', 'texte'),
                'supports'	            => array(
                                            'align'	=> false,
                                            'mode'  => true,
                                        ),
                'example'               => array(
                                            'attributes' => array(
                                                'mode' => 'preview',
                                                'data' => array(
                                                'is_preview'      => true,
                                                'names'           => "William",
                                                'text'            => "Lorem ispum dolores sid amet cumque.",
                                                )
                                            )
                                        )
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'liste-partenaires',
                'title'				    => __('Partenaires'),
                'description'		    => __('Liste de logos des partenaires. Lien vers le site du partenaire en option'),
                'placeholder'		    => __('Partenaires'),
                'render_template'	    => 'template-parts/block/liste-partenaires.php',
                'category'			    => 'anaya',
                'mode'                  => 'edit',
                'icon'				    => 'images-alt2',
                'keywords'			    => array('logo', 'lien', 'image', 'partenaire', 'liste'),
                'supports'	            => array(
                                            'align'	=> false,
                                            'mode'  => true,
                                        ),
                'example'               => array(
                                            'attributes' => array(
                                                'mode' => 'preview',
                                                'data' => array(
                                                'is_preview'      => true,
                                                'title'           => "Partenaires",
                                                )
                                            )
                                        )
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'membre-equipe',
                'title'				    => __('Membre de l\'équipe'),
                'description'		    => __('Présentation d\'un membre de l\'équipe : nom, age, photo et texte'),
                'placeholder'		    => __('Équipe'),
                'render_template'	    => 'template-parts/block/membre-equipe.php',
                'category'			    => 'anaya',
                'mode'                  => 'edit',
                'icon'				    => 'admin-users',
                'keywords'			    => array('nom', 'age', 'photo', 'description'),
                'supports'	            => array(
                                            'align'	=> false,
                                            'mode'  => true,
                                        ),
                'example'               => array(
                                            'attributes' => array(
                                                'mode' => 'preview',
                                                'data' => array(
                                                'is_preview'      => true,
                                                'name'            => "Nom",
                                                'age'             => "35 ans",
                                                'description'     => "Lorem ispum dolores sid amet cumque.",
                                                )
                                            )
                                        )
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'faq',
                'title'				    => __('FAQ'),
                'description'		    => __('Mise en place de questions/réponses pour la faq'),
                'placeholder'		    => __('faq'),
                'render_template'	    => 'template-parts/block/faq.php',
                'category'			    => 'anaya',
                'mode'                  => 'edit',
                'icon'				    => 'admin-users',
                'keywords'			    => array('faq', 'question', 'réponse'),
                'supports'	            => array(
                                            'align'	=> false,
                                            'mode'  => true,
                                        ),
               
            )
        );

    }
}