<?php
/* joints Custom Post Type Example
This page walks you through creating
a custom post type and taxonomies. You
can edit this one or copy the following code
to create another one.

I put this in a separate file so as to
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/


// ----------------------------------------------------------------------------Custom Post Types start----------------------------------------------------------------------------
// from https://capitainewp.io/formations/developper-theme-wordpress/creer-cpt-theme/ & https://developer.wordpress.org/reference/functions/register_post_type/

/* Custom Post Type Start */
function add_cpt() {

  // Add CPT named 'News': register_post_type('slug', array)
  register_post_type( 'news',  
    array(
      // labels = sentences used in cpt admin
      $labels = array(
        'name' => 'News',
        'all_items' => 'Tous les projets',  
        'singular_name' => 'Projet',
        'add_new_item' => 'Ajouter un projet',
        'edit_item' => 'Modifier le projet',
        'menu_name' => 'News',
        // Others
        // 'name_admin_bar' => '',
        // 'add_new' => '',
        // 'add_new_item' => '',
        // 'new_item' => '',
        // 'view_item' => '',
        // 'all_items' => '',
        // 'search_items' => '',
        // 'not_found' => '',
      ),
    
      // list all supports : title, editor, author, thumbnail, excerpt, comments, revisions, custom-fields, page-attributes, post-formats
      $supports = array( 'title', 'editor','thumbnail' ),
      
      'labels' => $labels,
      'supports' => $supports,
      // 'description'=> 'type the CPT description here',
      'public' => true,
      'show_in_rest' => true,
      // 'has archive' => true if it's like an article with 'archive' & 'single'
      'has_archive' => true,
      
      'menu_position' => 5, 
      'menu_icon' => 'dashicons-admin-customizer',
    )
  );


}
// Hooking up our function to theme setup
// add_action( 'init', 'add_cpt' );
  

// ----------------------------------------------------------------------------Custom Post Types end----------------------------------------------------------------------------




/*
 * Initialise les Custom Post-Types créés
 * ----------------------------------------------------------------------------*/
// add_action( 'init', 'initCustomPostTypes' );
function initCustomPostTypes() {

  // REUSSITES
  $slug = "solution";
  $slug_rewrite = basename(get_permalink(get_field("archive_solution", "options")));
  $args = array(
    'labels'             => defineCPTLabels($slug, "Solution", "Solutions"),
    'description'        => '',
  	'public'             => true,
  	'publicly_queryable' => true,
  	'show_ui'            => true,
  	'show_in_menu'       => true,
  	'query_var'          => true,
  	'has_archive'        => false,
  	'hierarchical'       => false,
  	'show_in_rest'       => true,
    'rest_base'          => false,
  	'menu_position'      => null,
  	'capability_type'    => 'post',
    'taxonomies'         => array('post_tag'),
  	'supports'           => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'revision'),
    'menu_icon'          => 'dashicons-awards',
    'rewrite'            => array('slug' => $slug_rewrite, 'hierarchical' => false, 'with_front' => false),
  );
  //register_post_type($slug, $args);
  // ---------------------------------------------------------------------------

}


/*
 * Ajuste le comportement par défaut des Post-Types par défaut : pages et posts
 * ----------------------------------------------------------------------------*/
//add_action( 'init', 'my_add_template_to_posts' );
function my_add_template_to_posts() {
  $post_type_object = get_post_type_object('post');
  $slug_rewrite = basename(get_permalink(get_field("archive_post", "options")));
  $post_type_object->rewrite = array('slug' => $slug_rewrite, 'hierarchical' => false, 'with_front' => false);
}




/*
 * Défini les labels pour le Custom Post-Type en paramètre
 * ----------------------------------------------------------------------------*/
function defineCPTLabels($slug, $name, $names, $feminin = false) {

  if ( $feminin ) :
    $un = "une";
    $nouv = "nouvelle";
    $auc = "aucune";
    $trouv = "trouvée";
    $all = "Toutes";
    $parent = "parente";
  else :
    $un = "un";
    $nouv = "nouveau";
    $auc = "aucun";
    $trouv = "trouvé";
    $all = "Tous";
    $parent = "parent";
  endif;

  $labels = array(
  	'name'               => __($names, 'nbtheme'),
  	'singular_name'      => __($name, 'nbtheme'),
  	'menu_name'          => __($names, 'nbtheme'),
  	'name_admin_bar'     => __($names, 'nbtheme'),
  	'add_new'            => __('Ajouter '.$un.' '.$nouv, 'nbtheme'),
  	'add_new_item'       => __('Ajouter '.$un.' '.strtolower($name), 'nbtheme'),
  	'new_item'           => __(ucfirst($nouv).' '.strtolower($name), 'nbtheme'),
  	'edit_item'          => __('Éditer', 'nbtheme'),
    'update_item'        => __('Mettre à jour', 'nbtheme'),
  	'view_item'          => __('Voir', 'nbtheme'),
  	'all_items'          => __('Tous les '.strtolower($names), 'nbtheme'),
  	'search_items'       => __('Recherche '.$un.' '.strtolower($name), 'nbtheme'),
  	'parent_item'        => __(ucfirst($name).' parent :', 'nbtheme'),
  	'parent_item_colon'  => __(ucfirst($name).' parent :', 'nbtheme'),
  	'not_found'          => __(ucfirst($auc).' '.strtolower($name).' '.$trouv.'.', 'nbtheme'),
  	'not_found_in_trash' => __(ucfirst($auc).' '.strtolower($name).' '.$trouv.' dans la corbeille.', 'nbtheme')
  );

  return $labels;
}

function pluginize_local_cptui_data( $data = array() ) {
  $theme_dir = get_stylesheet_directory();
  // Create our directory if it doesn't exist.
  if ( ! is_dir( $theme_dir .= '/cptui_data' ) ) {
      mkdir( $theme_dir, 0755 );
  }

  if ( array_key_exists( 'cpt_custom_post_type', $data ) ) {
      // Fetch all of our post types and encode into JSON.
      $cptui_post_types = get_option( 'cptui_post_types', array() );
      $content = json_encode( $cptui_post_types );
      // Save the encoded JSON to a primary file holding all of them.
      file_put_contents( $theme_dir . '/cptui_post_type_data.json', $content );
  }

  if ( array_key_exists( 'cpt_custom_tax', $data ) ) {
      // Fetch all of our taxonomies and encode into JSON.
      $cptui_taxonomies = get_option( 'cptui_taxonomies', array() );
      $content = json_encode( $cptui_taxonomies );
      // Save the encoded JSON to a primary file holding all of them.
      file_put_contents( $theme_dir . '/cptui_taxonomy_data.json', $content );
  }
}
add_action( 'cptui_after_update_post_type', 'pluginize_local_cptui_data' );
add_action( 'cptui_after_update_taxonomy', 'pluginize_local_cptui_data' );