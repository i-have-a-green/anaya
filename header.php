<?php
/**
 * The header for our theme
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php 
	/*
	echo '<link rel="icon" type="image/png" href="'. get_template_directory_uri().'/favicon/favicon-16px.png" sizes="16x16">';
	echo '<link rel="icon" type="image/png" href="'. get_template_directory_uri().'/favicon/favicon-32px.png" sizes="32x32">';
	echo '<link rel="icon" type="image/png" href="'. get_template_directory_uri().'/favicon/favicon-96px.png" sizes="96x96">';
	echo '<link rel="icon" type="image/png" href="'. get_template_directory_uri().'/favicon/favicon-124px.png" sizes="124x124">';
	*/
	?>
	<link rel="icon" type="image/svg" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon.svg">
	<link rel="apple-touch-icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-124px.png" />
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/favicon/manifest.json">
	<meta name="theme-color" content="#ffffff">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<!-- Skip Links-->
	<a class="skip-link" tabindex="0"  href="#content"><?php esc_html_e( 'Accéder au contenu', 'ihag' ); ?></a>
	<a class="skip-link" tabindex="0" href="#menu"><?php esc_html_e( 'Menu', 'ihag' ); ?></a>
	<a class="skip-link" tabindex="0"  href="#footer"><?php esc_html_e( 'Accéder au pied de page', 'ihag' ); ?></a>

	<nav id="topbar" class="wrapper">

		<!-- Logo -->
		<a id="logo" calss="link-discrete black" href="<?php echo get_home_url(); ?>">
			<span class="sr-only"><?php esc_html_e('Lien vers la page d\'accueil', 'ihag')?></span>
			<?php 
			$logo = get_field('logo_header', 'options');
			if ($logo) {
				$size = 'logo';
				echo wp_get_attachment_image($logo, $size);
			} else {
				echo '<span class="h3-like black font-title"><'. get_bloginfo ( 'name' ) .'</span>';
			}
			?>
		</a>
		<div id="menu">
			<?php
			// Téléphone - Mobile only
			$phone = get_field('phone', 'options');
			if($phone) :
				echo '<a class="button-brd desktop-hidden huge-hidden" href="tel:'. $phone .'">';
					echo '<span class="sr-only">'; esc_html_e( 'Appeler le numéro de téléphone :', 'ihag' ); echo '</span>';
					echo $phone;
				echo '</a>';
			endif;
			?>
			<!-- Menu -->
			<a  href="<?php echo get_home_url();?>" class="desktop-hidden huge-hidden link">
				<span class="sr-only"><?php esc_html_e( 'Accéder à', 'ihag' ); ?></span>
				<?php esc_html_e( 'Accueil', 'ihag' ); ?>
			</a>
			<?php echo ihag_menu('primary'); ?>			
		</div>

		<div id="controls">
			<!-- Burger button -->
			<button id="burger-menu" class="reset-style desktop-hidden huge-hidden" onclick="toggleMenu()" aria-label="<?php esc_html_e( 'Ouvrir et fermer le menu', 'ihag' ); ?>">
				<span class="sr-only"><?php esc_html_e( 'Ouvrir et fermer le menu', 'ihag' ); ?></span>
				<img aria-hidden="true" src="<?php echo get_stylesheet_directory_uri(); ?>/image/burger.svg" height="18" width="24">
			</button>

			<?php
			// Téléphone
			$phone = get_field('phone', 'options');
			if($phone) :
				echo '<a id="phone-tab" class="button-brd desktop-hidden huge-hidden" href="tel:'. $phone .'">';
					echo '<span class="sr-only tablet-hidden mobile-hidden">'; esc_html_e( 'Appeler le numéro de téléphone :', 'ihag' ); echo $phone .'</span>';
					echo '<img aria-hidden="true" src="'. get_stylesheet_directory_uri() .'/image/phone.svg" height="34" width="34">';
				echo '</a>';
			endif;
			?>

			<?php 
				//Compte Woocommerce
				/*echo '<a class="link-discrete icon-right green" href="'. get_permalink( get_option('woocommerce_myaccount_page_id') ).'">';
					//echo '<span class="green tiny-hidden mobile-hidden">'; esc_html_e( 'Mon compte', 'ihag' ); echo '</span>';
					echo '<img src="'. get_stylesheet_directory_uri() .'/image/compte.svg" alt="Panier" height="24" width="24">';
				echo '</a>';*/
				echo '<a class="link-discrete icon-right green" href="'. get_permalink( wc_get_page_id( 'cart' ) ).'">';
					echo '<img src="'. get_stylesheet_directory_uri() .'/image/basket.svg" alt="Panier" height="24" width="24">';
					global $woocommerce;
					if(!is_cart()):
					echo '<div id="countCart">'.$woocommerce->cart->cart_contents_count.'</div>';
					endif;
				echo '</a>';
				
			?>
		</div>

		<?php echo ihag_menu('shop'); ?>

	</nav>

	<!-- #content -->
	<div id="content">
