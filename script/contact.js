if (document.forms.namedItem("contactForm")) {
    document.forms.namedItem("contactForm").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("contactForm");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'contactForm', true);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessage').disabled = false;
                document.getElementById('sendMessage').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessage').classList.add("showResponseMessage");
            }
        };
        xhr.send(formData);
    });
}



// if (document.forms.namedItem("footer-newslettter-form")) {
//     document.forms.namedItem("footer-newslettter-form").addEventListener('submit', function(e) {
//         document.getElementById('sendMessageNewsletter').disabled = true; //limit le problème des doubles clic
//         e.preventDefault();

//         var form = document.forms.namedItem("footer-newslettter-form");
//         var formData = new FormData(form);
//         xhr = new XMLHttpRequest();
//         xhr.open('POST', resturl + 'formNewsletter', true);
//         xhr.onload = function() {
//             if (xhr.status === 200) {
//                 document.getElementById('sendMessageNewsletter').disabled = false;
//                 document.getElementById('sendMessageNewsletter').classList.add("hiddenSubmitButton");
//                 document.getElementById('ResponseMessageNewsletter').innerText = xhr.response.replace('"', '').replace('"', '');
//                 document.getElementById('ResponseMessageNewsletter').classList.add("showResponseMessage");
//             }
//         };
//         xhr.send(formData);
//     });
// }