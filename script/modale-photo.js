document.addEventListener('DOMContentLoaded', function() {
    img_photo_mariage();
});


function img_photo_mariage() {
    var increment = 0;
    var els = document.getElementsByClassName("img_photo_mariage");
    Array.prototype.forEach.call(els, function(el) {

        el.addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();

            close_modale();
            var modale = document.createElement("div");
            modale.classList.add("modale_photo");
            //modale.onclick = close_modale;

            var button = document.createElement("button");
            button.classList.add("closeModale");
            button.onclick = close_modale;
            button.appendChild(document.createTextNode("X"));
            modale.appendChild(button);

            var img = document.createElement("img");
            img.src = el.dataset.src;
            img.alt = el.alt;
            img.id = "photoModale";
            modale.appendChild(img);

            var prevButton = document.createElement("button");
            prevButton.id = "prevButton";
            prevButton.dataset.increment = el.dataset.increment;
            prevButton.dataset.jsonId = el.dataset.jsonId;
            prevButton.onclick = prevPhoto;
            prevButton.appendChild(document.createTextNode("<"));
            modale.appendChild(prevButton);

            var nextButton = document.createElement("button");
            nextButton.id = "nextButton";
            nextButton.dataset.increment = el.dataset.increment;
            nextButton.dataset.jsonId = el.dataset.jsonId;
            nextButton.onclick = nextPhoto;
            nextButton.appendChild(document.createTextNode(">"));
            modale.appendChild(nextButton);

            document.body.appendChild(modale);
        });
    });
}

function prevPhoto() {
    btn = document.getElementById(this.dataset.jsonId);
    increment = parseInt(this.dataset.increment) - 1;
    document.getElementById("nextButton").dataset.increment = increment;
    document.getElementById("prevButton").dataset.increment = increment;

    var list_photos = JSON.parse(btn.dataset.json);
    var photo = list_photos[increment];
    document.getElementById("photoModale").src = photo.srclarge;

    document.getElementById("nextButton").disabled = false;
    document.getElementById("prevButton").disabled = false;
    if (increment <= 0) {
        this.disabled = true;
    }
}

function nextPhoto() {
    btn = document.getElementById(this.dataset.jsonId);
    increment = parseInt(this.dataset.increment) + 1;
    document.getElementById("nextButton").dataset.increment = increment;
    document.getElementById("prevButton").dataset.increment = increment;

    var list_photos = JSON.parse(btn.dataset.json);
    var photo = list_photos[increment];
    document.getElementById("photoModale").src = photo.srclarge;

    document.getElementById("nextButton").disabled = false;
    document.getElementById("prevButton").disabled = false;
    if (increment >= (list_photos.length - 1)) {
        this.disabled = true;
    }
}

function close_modale() {
    if (document.getElementsByClassName("modale_photo").length > 0) {
        var modale_photo = document.getElementsByClassName("modale_photo");
        modale_photo[0].remove();
    }
}