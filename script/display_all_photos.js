document.addEventListener('DOMContentLoaded', function() {
    var js_upload_photos = document.getElementsByClassName('js_upload_photos');

    Array.prototype.forEach.call(js_upload_photos, function(bloc) {
        var btn = bloc.getElementsByClassName('more_photos');
        btn = btn[0];
        var new_photos = document.getElementById("img-listing_" + btn.id);
        console.log(btn.dataset.json);
        list_photos = JSON.parse(btn.dataset.json);
        new_photos.innerHTML = '';
        var increment = 0;
        if (list_photos.length > 0) {

            //à adapter
            var nbrPhoto = 3;
            if (window.matchMedia("(min-width: 600px)").matches) {
                /* La largeur minimum de l'affichage est 600 px inclus */
                nbrPhoto = 3;
            } else {
                /* L'affichage est inférieur à 600px de large */
                nbrPhoto = 2;
            }

            list_photos.forEach(function(photo) {

                if (increment < nbrPhoto) {
                    var image = document.createElement('img');
                    image.src = photo.src;
                    image.alt = photo.alt;
                    image.classList.add("img_photo_mariage");
                    image.dataset.src = photo.srclarge;
                    image.dataset.increment = increment;
                    image.dataset.jsonId = btn.id;
                    new_photos.appendChild(image);
                }
                increment++;
            });
        }
        img_photo_mariage();
    });


    Array.prototype.forEach.call(js_upload_photos, function(bloc) {
        var btn = bloc.getElementsByClassName('more_photos');
        btn = btn[0];
        btn.addEventListener('click', function(e) {
            btn.style.display = "none";
            btn.parentNode.classList.add("show-second-button");
            btn.parentNode.parentNode.classList.add("show-legend");
            list_photos = JSON.parse(btn.dataset.json);
            var new_photos = document.getElementById("img-listing_" + btn.id);
            new_photos.innerHTML = '';
            var increment = 0;
            if (list_photos.length > 0) {
                list_photos.forEach(function(photo) {
                    var image = document.createElement('img');
                    image.src = photo.src;
                    image.alt = photo.alt;
                    image.classList.add("img_photo_mariage");
                    image.dataset.src = photo.srclarge;
                    image.dataset.increment = increment;
                    image.dataset.jsonId = btn.id;
                    new_photos.appendChild(image);
                    increment++;
                });
            }
            img_photo_mariage();
        });

    });


});