/*

Animate header.php (see header.scss)

File structure :
------------------
1 - Open & Close menu
------------------
2 - Detect click on #overlay
------------------
3 - Scroll detection
------------------
// 4 - Modif topbar background on scroll

*/

// 1 - Open & Close menu (for mobile devices)
function toggleMenu() {
    document.getElementById("menu").classList.toggle("menu-open");
}

// 2 - Detect click on #overlay (for mobile menu)
// When we click outside of the mobile menu, the menu disappears
// We use #overlay to detect the click

/*document.addEventListener("DOMContentLoaded", function(event) {
    document.getElementById("overlay").addEventListener("click", function(e) {
        toggleMenu ();
    });
});*/

// 3 - Scroll detection
//topbar gets sticky on scroll
var scroll = document.getElementById("menu");
var menu = document.getElementById("menu");
var sticky = 0;
var isWhite = menu.dataset.white;

window.onscroll = function() {

    if (window.pageYOffset > sticky) {
        menu.classList.add("menu-small");
    } else {
        menu.classList.remove("menu-small");
    }
};


// 4 - Modif topbar background on scroll

/*window.onscroll = function() {

    var menu = document.getElementById("scroll-anchor");
    var header = document.getElementById("topbar-wrapper");
    var sticky = menu.offsetTop;

    if (window.pageYOffset > sticky) {
        header.classList.add("scroll");
        window.onscroll = function() { return; };
    } else {
        header.classList.remove("scroll");
    }
};*/

// var derniere_position_de_scroll_connue = 0;
// var ticking = false;
// var menu = document.getElementById("scroll-anchor");
// var header = document.getElementById("topbar-wrapper");
// var sticky = menu.offsetTop;

// window.addEventListener('scroll', function(e) {
//     if (!ticking) {
//         window.setTimeout(function() {
//             ticking = false;
//             if (window.scrollY > sticky) {
//                 header.classList.add("scroll");
//             } else {
//                 header.classList.remove("scroll");
//             }
//         }, 300); //fréquence du scroll
//     }
//     ticking = true;
// });